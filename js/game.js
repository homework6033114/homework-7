class GameField {
  constructor(state, mode, gameFieldStatus, isOverGame) {
    this.state = [null, null, null, null, null, null, null, null, null]
    this.mode = 'X'
    this.gameFieldStatus = ''
    this.isOverGame = false
  }

  setMode() {
    this.mode === 'X' ? (this.mode = 'O') : (this.mode = 'X')
  }

  getGameFieldStatus() {
    const checkWinner = (cells) => {
      const winCombinations = [
        // Rows
        ['0', '1', '2'],
        ['3', '4', '5'],
        ['6', '7', '8'],
        // Columns
        ['0', '3', '6'],
        ['1', '4', '7'],
        ['2', '5', '8'],
        // Diagonal
        ['0', '4', '8'],
        ['2', '4', '6'],
      ]

      for (let comb of winCombinations) {
        if (
          cells[comb[0]] !== null &&
          cells[comb[0]] === cells[comb[1]] &&
          cells[comb[1]] === cells[comb[2]]
        ) {
          return cells[comb[0]]
        }
      }
      return false
    }

    const checkTie = (cells) => {
      for (let cell of cells) {
        if (cell === null) {
          return false
        }
      }
      return true
    }

    const winner = checkWinner(this.state)

    if (winner) {
      console.log(
        (this.gameFieldStatus = `${
          winner === 'X' ? 'Крестики победили' : 'Нолики победили'
        }`)
      )
    } else if (checkTie(this.state)) {
      console.log((this.gameFieldStatus = 'Ничья'))
    }
  }

  getFieldCellValue(i) {
    this.state[i] = this.mode
  }
}
const gameField = new GameField()
const gameStep = document.querySelector('.game-step')
const cells = document.querySelectorAll('.cell')

// Отображение символа X или O на поле
const displayCurrentMode = (div, mode) => {
  let element = document.createElement('img')
  element.src = mode === 'X' ? '../imgs/xxl-x.svg' : '../imgs/xxl-zero.svg'
  div.appendChild(element)
}

// Отображение символа X или O в нижней части интерфейса
const displayCurrentPlayer = (mode) => {
  gameStep.innerHTML = `
  Ходит &nbsp;
  ${
    mode === 'X'
      ? '<img src="../imgs/x.svg" />'
      : '<img src="../imgs/zero.svg" />'
  } &nbsp; Игрок`
}

const play = (e) => {
  const clickedCell = e.target
  const clickedCellIndex = +clickedCell.getAttribute('data-cell-index')

  if (gameField.state[clickedCellIndex] !== null || gameField.isOverGame) {
    return
  }
  displayCurrentMode(clickedCell, gameField.mode)

  gameField.getFieldCellValue(clickedCellIndex)

  gameField.getGameFieldStatus()

  if (gameField.gameFieldStatus) {
    gameField.isOverGame = true
  } else {
    gameField.setMode()
    displayCurrentPlayer(gameField.mode)
  }
}
displayCurrentPlayer(gameField.mode)
cells.forEach((cell) => {
  cell.addEventListener('click', play)
})
